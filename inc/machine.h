#pragma once

#include "z80emu.h"
#include "z80user.h"

void InitMachine(void);
void LoadBootSector(void);
void RunMachine(void);

extern MACHINE machine;
