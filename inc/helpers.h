#pragma once

#include "board.h"

static const char *initSTR = "\r"
		"z80emu Version 1.1.3 Copyright (c) 2012-2017 Lin Ke-Fong\r\n"
		"Host: LPC824M201JHI33\r\n"
		"Arch: ARMv6-M Cortex-M0+ (Thumb-1) (Thumb-2)\r\n"
		"Total Flash: 32768\r\n"
		"Total RAM: 8192\r\n"
		"USART: 3\r\n"
		"I2C: 4\r\n"
		"SPI: 2\r\n"
		"ADC: 12 Channels\r\n"
		"GPIO: 29\r\n"
		"Package: HVQFN33\r\n"
		"\r\n"
		"Systick Enabled\r\n"
		"Library: Redlib (nohost)\r\n"
		"Ext RAM: 65535 bytes\r\n"
		"\r\n"
		"Memory region         Used Size  Region Size  %age Used\r\n"
		"        MFlash32:         18 KB        32 KB     56.25%\r\n"
		"         RamLoc8:        5976 B         8 KB     72.95%\r\n"
		"\r\n";

static const char *CRLF = "\r\n";

void print_sys(void);

void print_int(const int);
void print_uint8_hex(uint8_t);

void print_uint32_t(uint32_t);


extern uint32_t red_ct;
extern uint32_t green_ct;
