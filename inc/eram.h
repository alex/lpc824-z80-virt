#pragma once

#include "board.h"

#define ER_DATA_0 0
#define ER_DATA_1 13
#define ER_DATA_2 14
#define ER_DATA_3 19
#define ER_DATA_4 20
#define ER_DATA_5 21
#define ER_DATA_6 22
#define ER_DATA_7 23
#define ER_ADDR_LATCH 17
#define ER_OE_N 28
#define ER_WE_N 6

void Init_GPIO(void);

uint8_t get_data_byte(void);
void set_data_byte(const uint8_t);
uint8_t eram_read(const uint16_t);
void eram_write(const uint16_t, const uint8_t);
void ramtest(const uint8_t);
