#pragma once

#include "board.h"

#define SPI_BUF_SIZE 262

extern SPI_DATA_SETUP_T XfSetup;
extern uint16_t TxBuf[SPI_BUF_SIZE];
extern uint16_t RxBuf[SPI_BUF_SIZE];

void Init_SPI(void);

void Floppy_to_ERAM(const uint8_t, const uint8_t, const uint8_t, const uint16_t);
void ERAM_to_Floppy(const uint16_t, const uint8_t, const uint8_t, const uint8_t);
