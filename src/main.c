#include "board.h"

#include <cr_section_macros.h>
#include <machine.h>

#include "eram.h"
#include "flash.h"

#include "helpers.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define TICKRATE_HZ        100
#define	EV_TICK_CT_DISPLAY 0x01

#define	LED_RED   0
#define	LED_GREEN 1
#define	LED_BLUE  2

static inline void ansi_clr_screen(void)
{
	Board_UARTPutSTR("\x1B[2J\x1B[0m\x1B[1;1H");
}

static inline void Board_LED_clear(void)
{
	Board_LED_Set(LED_RED, false);
	Board_LED_Set(LED_GREEN, false);
	Board_LED_Set(LED_BLUE, false);
}

/**
 * @brief	Handle interrupt from SysTick timer
 * @return	Nothing
 */
static uint32_t sys_event = 0;
static uint32_t tick_ct = 0;
uint32_t red_ct = 0;
uint32_t green_ct = 0;

void SysTick_Handler(void)
{
	tick_ct += 1;
	//Board_LED_Set(LED_BLUE, !(tick_ct % 4));

	if (red_ct != 0) {
		if (red_ct == 1) Board_LED_Set(LED_RED, false);
		--red_ct;
	}
	if (green_ct != 0) {
		if (green_ct == 1) Board_LED_Set(LED_GREEN, false);
		--green_ct;
	}

	// if ((tick_ct % 100) == 0) sys_event |= EV_TICK_CT_DISPLAY; We are not using this now
}


/**
 * @brief	main routine for blinky example
 * @return	Function should not exit.
 */
int main(void) {
	SystemCoreClockUpdate();
	Board_Init();

	Init_GPIO();
	Init_SPI();

	Board_LED_clear();
	ansi_clr_screen();

	Board_UARTPutSTR(initSTR);

	print_sys();

	Board_UARTPutSTR(CRLF);

	/* Enable SysTick Timer */
	SysTick_Config(SystemCoreClock / TICKRATE_HZ);

	//Board_UARTPutSTR("Running RAM test.");
	//ramtest(0x55);
	//ramtest(0x00);
	//Board_UARTPutSTR(CRLF);
	//Board_UARTPutSTR(CRLF);

	XfSetup.pTx = TxBuf; XfSetup.pRx = RxBuf;
	XfSetup.RxCnt = XfSetup.TxCnt = 0;
	XfSetup.DataSize = 8;
	XfSetup.Length = 8;
	TxBuf[0] = 0xAB;
	Chip_SPI_RWFrames_Blocking(LPC_SPI1, &XfSetup);
	Board_UARTPutSTR((RxBuf[4] == 0x13) ? "1048576 bytes of SPI Flash initialized successfully." : "Error initializing SPI Flash!");
	Board_UARTPutSTR(CRLF);
	Board_UARTPutSTR(CRLF);

	RunMachine();

	/* DeInitialize SPI peripheral */
	Chip_SPI_DeInit(LPC_SPI1);

    while(1) {
		__WFI();
		if (sys_event & EV_TICK_CT_DISPLAY) {
			sys_event &= ~EV_TICK_CT_DISPLAY;
			Board_UARTPutSTR("\rsystk: ");
			static char out_str[16];
			Board_itoa(tick_ct, out_str, 10);
			Board_UARTPutSTR(out_str);
		}
    }

    return 0;
}
