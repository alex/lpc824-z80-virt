#include "eram.h"

#include "board.h"

static const uint32_t data_pins = 0x00F86001;

void Init_GPIO(void)
{
	/* Enable clock to switch matrix so we can configure the matrix */
	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_SWM);
	/* By default, all analog pins are disabled, but we will disable them anyway */
	Chip_SWM_DisableFixedPin(SWM_FIXED_ACMP_I1);
	Chip_SWM_DisableFixedPin(SWM_FIXED_ACMP_I3);
	Chip_SWM_DisableFixedPin(SWM_FIXED_ACMP_I4);
	Chip_SWM_DisableFixedPin(SWM_FIXED_VDDCMP);
	Chip_SWM_DisableFixedPin(SWM_FIXED_ADC11);
	Chip_SWM_DisableFixedPin(SWM_FIXED_ADC7);
	Chip_SWM_DisableFixedPin(SWM_FIXED_ADC6);
	Chip_SWM_DisableFixedPin(SWM_FIXED_ADC8);
	Chip_SWM_DisableFixedPin(SWM_FIXED_ADC9);
	/* Turn clock to switch matrix back off to save power */
	Chip_Clock_DisablePeriphClock(SYSCTL_CLOCK_SWM);

	/* Set port 0 pins 6, 17, 28 to the output direction*/
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0, ER_ADDR_LATCH);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0, ER_OE_N);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0, ER_WE_N);

	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, ER_ADDR_LATCH, false);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, ER_OE_N, true);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, ER_WE_N, true);

	Chip_GPIO_SetPortDIROutput(LPC_GPIO_PORT, 0, data_pins);

	/* Set GPIO port mask value to make sure only data pins are active during state change */
	Chip_GPIO_SetPortMask(LPC_GPIO_PORT, 0, ~data_pins);
}

uint8_t get_data_byte(void)
{
	uint32_t portValue = Chip_GPIO_GetMaskedPortValue(LPC_GPIO_PORT, 0);

	return (portValue & 0x0000001)
			| ((portValue & 0x00006000) >> 12)
			| ((portValue & 0x00F80000) >> 16);
}

void set_data_byte(const uint8_t data)
{
	uint32_t portValue = ((data & 0xF8) << 16)
			| ((data & 0x06) << 12)
			| (data & 0x01);

	Chip_GPIO_SetMaskedPortValue(LPC_GPIO_PORT, 0, portValue);
}

uint8_t eram_read(const uint16_t addr)
{
	// latch the high address
	set_data_byte(addr >> 8);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, ER_ADDR_LATCH, true);

	// latch the low address
	set_data_byte(addr & 0x00FF);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, ER_ADDR_LATCH, false);

	// activate RAM
	Chip_GPIO_SetPortDIRInput(LPC_GPIO_PORT, 0, data_pins); // input data
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, ER_OE_N, false);
	uint8_t data = get_data_byte();
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, ER_OE_N, true);
	Chip_GPIO_SetPortDIROutput(LPC_GPIO_PORT, 0, data_pins); // output data

	return data;
}

void eram_write(const uint16_t addr, const uint8_t data)
{
	// latch the high address
	Chip_GPIO_SetPortDIROutput(LPC_GPIO_PORT, 0, data_pins); // output data
	set_data_byte(addr >> 8);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, ER_ADDR_LATCH, true);

	// latch the low address
	set_data_byte(addr & 0x00FF);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, ER_ADDR_LATCH, false);

	// activate RAM
	set_data_byte(data);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, ER_WE_N, false);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, ER_WE_N, true);
}

void ramtest(const uint8_t pattern)
{
	int error, buf;

	Board_UARTPutSTR("\r\nTesting pattern 0x");
	print_uint8_hex(pattern);
	Board_UARTPutSTR("...");
	for (unsigned int i = 0; i < 65536; i++) {
		eram_write(i, pattern);
	}
	Board_UARTPutSTR("Verifying...");
	error = 0;
	for (unsigned int i = 0; i < 65536; i++) {
		buf = eram_read(i);
		if (buf != pattern) {
			++error;
		}
	}
	if (error) {
		print_int(error);
		Board_UARTPutSTR(" errors detected");
	} else {
		Board_UARTPutSTR("passed!");
	}
}
