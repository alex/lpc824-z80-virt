#include "flash.h"

#include "board.h"
#include "eram.h"

SPI_DATA_SETUP_T XfSetup;
uint16_t TxBuf[SPI_BUF_SIZE];
uint16_t RxBuf[SPI_BUF_SIZE];

uint8_t DiskBuf[4096];

void Init_SPI(void)
{
	/* Configure pin muxing */
	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_SWM);
	Chip_SWM_MovablePinAssign(SWM_SPI1_SSEL0_IO, 15);
	Chip_SWM_MovablePinAssign(SWM_SPI1_SCK_IO, 24);
	Chip_SWM_MovablePinAssign(SWM_SPI1_MISO_IO, 25);
	Chip_SWM_MovablePinAssign(SWM_SPI1_MOSI_IO, 26);
	Chip_Clock_DisablePeriphClock(SYSCTL_CLOCK_SWM);

	/* Initialize SPI buffers */
	for (unsigned int i = 0; i < SPI_BUF_SIZE; ++i) {
		TxBuf[i] = 0;
		RxBuf[i] = 0;
	}

	/* Initialize SPI peripheral */
	Chip_SPI_Init(LPC_SPI1);
	Chip_SPI_ConfigureSPI(LPC_SPI1, SPI_MODE_MASTER | /* Enable master/Slave mode */
						  SPI_CLOCK_CPHA0_CPOL0 |     /* Set Clock polarity to 0 */
						  SPI_CFG_MSB_FIRST_EN |      /* Enable MSB first option */
						  SPI_CFG_SPOL_LO);           /* Chipselect is active low */

	static SPI_DELAY_CONFIG_T DelayConfigStruct;
	DelayConfigStruct.FrameDelay = 0;
	DelayConfigStruct.PostDelay = 0;
	DelayConfigStruct.PreDelay = 0;
	DelayConfigStruct.TransferDelay = 0;
	Chip_SPI_DelayConfig(LPC_SPI1, &DelayConfigStruct);

	Chip_SPI_Enable(LPC_SPI1);
}

static unsigned int SPI_Read_Status_Reg_1(void)
{
	XfSetup.Length = 2;
	XfSetup.pTx = TxBuf;
	XfSetup.pRx = RxBuf;
	XfSetup.RxCnt = XfSetup.TxCnt = 0;
	XfSetup.DataSize = 8;

	TxBuf[0] = 0x05; // Read Status Register-1 (6.2.8)

	Chip_SPI_RWFrames_Blocking(LPC_SPI1, &XfSetup);

	return RxBuf[1];
}

static inline void SPI_Set_Write_Enable_Latch(void) {
	// wait for flash memory to be NOT busy
	while (SPI_Read_Status_Reg_1() & 0x01);

	// set write enable latch
	XfSetup.pRx = RxBuf; XfSetup.pTx = TxBuf;
	XfSetup.RxCnt = XfSetup.TxCnt = 0;
	XfSetup.DataSize = 8;
	XfSetup.Length = 1;

	TxBuf[0] = 0x06; // Write Enable (6.2.5)

	Chip_SPI_RWFrames_Blocking(LPC_SPI1, &XfSetup);
}

/**
 * Read 'length'-bytes sector from the emulated floppy
 */
static uint16_t* SPI_Read_Sector(const uint32_t start, const uint32_t length)
{
	// wait for flash memory to be NOT busy
	while (SPI_Read_Status_Reg_1() & 0x01);

	XfSetup.pRx = RxBuf; XfSetup.pTx = TxBuf;
	XfSetup.RxCnt = XfSetup.TxCnt = 0;
	XfSetup.DataSize = 8;
	XfSetup.Length = 1 + 3 + 1 + length;

	TxBuf[0] = 0x0B; // Fast Read (6.2.11)
	TxBuf[1] = (start >> 16) % 256;
	TxBuf[2] = (start >>  8) % 256;
	TxBuf[3] = (start >>  0) % 256;
	TxBuf[4] = 0; // dummy byte

	Chip_SPI_RWFrames_Blocking(LPC_SPI1, &XfSetup);

	return RxBuf + 5;
}
/**
 * Read a 128-byte sector from the emulated floppy
 */
static void SPI_Write_Sector(const uint32_t start)
{
	// set write enable latch (also waits for not busy)
	SPI_Set_Write_Enable_Latch();

	// write sector
	XfSetup.pRx = RxBuf; XfSetup.pTx = TxBuf;
	XfSetup.RxCnt = XfSetup.TxCnt = 0;
	XfSetup.DataSize = 8;
	XfSetup.Length = 1 + 3 + 256;

	TxBuf[0] = 0x02; // Page Program (6.2.21)
	TxBuf[1] = (start >> 16) % 256;
	TxBuf[2] = (start >>  8) % 256;
	TxBuf[3] = (start >>  0) % 256;

	Chip_SPI_RWFrames_Blocking(LPC_SPI1, &XfSetup);
}

static void SPI_Erase(const uint32_t address)
{
	// set write enable latch (also waits for not busy)
	SPI_Set_Write_Enable_Latch();

	XfSetup.pRx = RxBuf; XfSetup.pTx = TxBuf;
	XfSetup.RxCnt = XfSetup.TxCnt = 0;
	XfSetup.DataSize = 8;
	XfSetup.Length = 4;

	TxBuf[0] = 0x20; // Sector Erase (6.2.23)
	TxBuf[1] = (address >> 16) % 256;
	TxBuf[2] = (address >>  8) % 256;
	TxBuf[3] = (address >>  0) % 256;

	Chip_SPI_RWFrames_Blocking(LPC_SPI1, &XfSetup);
}

/**
 * Read a 128-byte sector from the emulated floppy
 */
void Floppy_to_ERAM(const uint8_t disk, const uint8_t track, const uint8_t sector, const uint16_t mdest)
{
	// 128 bytes per sector
	// 26 sectors per track
	// 77 tracks per disk
	// 243 kiB per disk

	const uint32_t lba = (((uint32_t)disk * 77) + (uint32_t)track) * 26 + (uint32_t)sector;
	const uint32_t starting_addr = lba * 128;

	uint16_t* data = SPI_Read_Sector(starting_addr, 128);

	// Write 128-byte sector to ERAM
	for (uint16_t count = 0; count < 128; ++count)
		eram_write((uint16_t)(mdest + count), data[count]);
}

/**
 * Write a 128-byte sector to the emulated floppy
 */
void ERAM_to_Floppy(const uint16_t msrc, const uint8_t disk, const uint8_t track, const uint8_t sector)
{
	const uint32_t lba = (((uint32_t)disk * 77) + (uint32_t)track) * 26 + (uint32_t)sector;
	const uint32_t starting_addr = lba * 128;
	const uint32_t group = starting_addr / 4096;
	const uint8_t position = lba % 32;
	const uint32_t group_base = group * 4096;

	// read entire 4096-byte group to SRAM
	for (uint8_t page = 0; page < 16; ++page) {
		// read the page
		uint16_t* data = SPI_Read_Sector(group_base + page * 256, 256);
		// copy the page to DiskBuf
		for (unsigned int i = 0; i < 256; ++i) {
			DiskBuf[page * 256 + i] = data[i];
		}
	}

	// patch the changed part of the data
	for (unsigned int i = 0; i < 128; ++i) {
		DiskBuf[position * 128 + i] = eram_read(msrc + i);
	}

	// erase the old data
	SPI_Erase(group_base);

	// write all of it back to flash
	for (uint8_t page = 0; page < 16; ++page) {
		// copy from DiskBuf to TxBuf
		for (unsigned int i = 0; i < 256; ++i) {
			TxBuf[i + 4] = DiskBuf[page * 256 + i];
		}

		SPI_Write_Sector(group_base + page * 256);
	}
}
