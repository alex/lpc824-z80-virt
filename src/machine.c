#include "machine.h"

#include "z80emu.h"

#include "board.h"
#include "eram.h"
#include "flash.h"
#include "helpers.h"

#define Z80_CPU_SPEED           4000000   /* In Hz. */
#define CYCLES_PER_STEP         (Z80_CPU_SPEED / 50)
#define MAXIMUM_STRING_LENGTH   100

/**
 * HELPFUL UTILITIES:

Copying code from byte array to external RAM:

	for (unsigned int i = 0; i < file_len; i++)
		eram_write(i + 0x0000, file[i]);

Format a disk

	for (unsigned int i = 0; i < 128; i++)
		eram_write(i + 0x0100, 0xE5);

Load BIOS and OS from SPI flash to ERAM
	for (unsigned int i = 0; i < cpm22_len; i++)
		eram_write(i + 0xE400, cpm22[i]);
	for (unsigned int i = 0; i < bios_len; i++)
		eram_write(i + 0xFA00, bios[i]);
	ERAM_to_Floppy(0x0000, 0, 0, 0);
	for (unsigned int i = 0; i < 51; i++)
		ERAM_to_Floppy(0x80 * i + 0xE400, 0, 0, i + 1);

 */
void FormatDisk(uint8_t num)
{
	for (unsigned int i = 0; i < 128; ++i)
		eram_write(i + 0x0100, ((i % 32 == 0) ? 0xE5 : 0xFF));
	for (unsigned int i = 0; i < 16; ++i)
		ERAM_to_Floppy(0x0100, num, 2, i);
	Board_UARTPutSTR("Format complete.\r\n\n");
}

void RunMachine(void)
{
	MACHINE context;

	// load coldboot from first sector of floppy
	Floppy_to_ERAM(0, 0, 0, 0x0000);
	Board_UARTPutSTR("Bootsector copied to RAM. 128 bytes at location $0000.\r\n\n");

	uint8_t ptr = 0;
	for (uint16_t c = 0; c < 0; ++c) {
		Floppy_to_ERAM(0, 0, ptr++, 0x0100);
		for (uint16_t i = 0x100; i < 0x180; ++i) {
			if (!((i) % 16)) {
				print_uint32_t(i);
				Board_UARTPutChar(' ');
				Board_UARTPutChar(' ');
			}
			print_uint8_hex(eram_read(i));
			Board_UARTPutChar(' ');
			if (!((i + 1) % 16))
				Board_UARTPutSTR("\r\n");
		}
	}

	context.is_done = 0;

	/* Emulate. */

	Z80Reset(&context.state);
	context.state.pc = 0x0000;

	Board_UARTPutSTR("Jumping to $0000...\r\n\r\n");

	int total = 0;
	do {
		total += Z80Emulate(&context.state, CYCLES_PER_STEP, &context);
	}
	while (!context.is_done);


	print_int(total); Board_UARTPutSTR(" cycle(s) emulated.\r\n");
	Board_UARTPutSTR("For a Z80 running at 4 MHz, that would be ");
	print_int((int)(total / Z80_CPU_SPEED));
	Board_UARTPutSTR(" second(s).\r\n");

    Board_UARTPutSTR("\r\nEmulation halted.\r\n");
}

void SystemCall(MACHINE *m, const uint8_t port, const uint16_t pc)
{
	/*
	Board_UARTPutSTR("\r\n\n*** SYSCALL "); print_int(port); Board_UARTPutSTR(" ***\r\n");
	Board_UARTPutSTR("A="); print_int(m->state.registers.byte[Z80_A]); Board_UARTPutSTR(" F="); print_int(m->state.registers.byte[Z80_F]); Board_UARTPutSTR(CRLF);
	Board_UARTPutSTR("B="); print_int(m->state.registers.byte[Z80_B]); Board_UARTPutSTR(" C="); print_int(m->state.registers.byte[Z80_C]); Board_UARTPutSTR(CRLF);
	Board_UARTPutSTR("D="); print_int(m->state.registers.byte[Z80_D]); Board_UARTPutSTR(" E="); print_int(m->state.registers.byte[Z80_E]); Board_UARTPutSTR(CRLF);
	Board_UARTPutSTR("H="); print_int(m->state.registers.byte[Z80_H]); Board_UARTPutSTR(" L="); print_int(m->state.registers.byte[Z80_L]); Board_UARTPutSTR(CRLF);
	Board_UARTPutSTR(CRLF);
	*/

	switch (port) {
	case 0: // CONST
		m->state.registers.byte[Z80_A] = (Chip_UART_GetStatus(DEBUG_UART) & UART_STAT_RXRDY) ? 0xFF : 0x00;
		break;
	case 1: // CONIN
		Chip_UART_ReadBlocking(DEBUG_UART, &(m->state.registers.byte[Z80_A]), 1);
		break;
	case 2: // CONOUT
		Board_UARTPutChar(m->state.registers.byte[Z80_C]);
		break;
	case 3: // CONSTR
		for (uint16_t i = m->state.registers.word[Z80_DE];; ++i) {
			uint8_t data = eram_read(i);
			if (data == 0) break;
			else Board_UARTPutChar(data);
		}
		break;
	case 4: // READ/WRITE
	{
		_Bool out = (m->state.registers.byte[Z80_B] & 0x80) != 0;
		uint8_t cnt = m->state.registers.byte[Z80_B] & 0x7F;
		uint8_t dsk = m->state.registers.byte[Z80_A];
		uint8_t trk = m->state.registers.byte[Z80_D];
		uint8_t sec = m->state.registers.byte[Z80_E];
		uint16_t dma = m->state.registers.word[Z80_HL];

		if (out) {
			Board_LED_Set(1, true);
			green_ct = 10;					// show green led for 100ms
		} else {
			Board_LED_Set(0, true);
			red_ct = 10;					// show red led for 100ms
		}

		for (uint8_t i = 0; i < cnt; ++i) {
			if (out) {
				ERAM_to_Floppy(dma, dsk, trk, sec);
				m->state.registers.byte[Z80_A] = 0; // Write OK (TODO: Add write-only detection)
			} else {
				Floppy_to_ERAM(dsk, trk, sec, dma);
				m->state.registers.byte[Z80_A] = 0; // Read OK
			}
			dma += 128; ++sec;
		}
	}
		break;
	default: // UNKNOWN
		Board_UARTPutSTR("syscall "); print_int(port); Board_UARTPutSTR(" @ 0x");
		{
			static char buf[5];
			Board_itoa(pc, buf, 16);
			Board_UARTPutSTR(buf);
		}
		Board_UARTPutSTR(CRLF);
	}
}
