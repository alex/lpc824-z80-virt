/*
 * helpers.c
 *
 *  Created on: Apr 11, 2020
 *      Author: ALZ
 */

#include "helpers.h"

#include "board.h"

static const char* hexstr = "0123456789ABCDEF";

__attribute__ ((section(".after_vectors")))
static void print_uint32_t_(uint32_t value, unsigned int bits_left)
{
	if (!bits_left) return;

	print_uint32_t_(value, bits_left - 4);
	Board_UARTPutChar(hexstr[(value >> (32 - bits_left)) % 16]);
}

__attribute__ ((section(".after_vectors")))
void print_uint32_t(uint32_t value)
{
	print_uint32_t_(value, 32);
}

/* Display system information */
__attribute__ ((section(".after_vectors")))
void print_sys(void)
{
	__disable_irq();
	Board_UARTPutSTR("System Clock: "); print_int(SystemCoreClock / 1000000); Board_UARTPutSTR(" MHz\r\n");
	Board_UARTPutSTR("Device ID: 0x"); print_uint32_t(Chip_SYSCTL_GetDeviceID()); Board_UARTPutSTR(CRLF);
	Board_UARTPutSTR("VTOR Address: 0x"); print_uint32_t((uint32_t) &(SCB->VTOR)); Board_UARTPutSTR(CRLF);
	Board_UARTPutSTR("CPU ID: 0x"); print_uint32_t((uint32_t) SCB->CPUID); Board_UARTPutSTR(CRLF);
	__enable_irq();
}

__attribute__ ((section(".after_vectors")))
static void print_int_(int n) {
  if (n <= -10) {
    print_int_(n / 10);
  }
  Board_UARTPutChar('0' - n % 10);
}

__attribute__ ((section(".after_vectors")))
void print_int(int n) {
  if (n < 0) {
    putchar('-');
  } else {
    n = -n;  // Make positive numbers, negative - no chance for overflow
  }
  print_int_(n);
  //Board_UARTPutChar('\n');
}

__attribute__ ((section(".after_vectors")))
void print_uint8_hex(uint8_t n) {
	Board_UARTPutChar(hexstr[n / 16]);
	Board_UARTPutChar(hexstr[n % 16]);
}
